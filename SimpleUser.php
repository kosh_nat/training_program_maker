<?php
/**
 * Created by PhpStorm.
 * User: knv
 * Date: 01.03.2018
 * Time: 14:29
 */

class SimpleUser extends User
{
    public $age = null;
    public $sex = null;
    public $weight = null;
    public $height = null;
    public $BMI = null;
    public $training_level = null;
    public $maternity = false;

    public function __construct($array) {
        $this->age = $array[0];
        $this->sex = $array[1];
        $this->weight = $array[2];
        $this->height = $array[3];
        $this->training_level = $array[4];
    }

    public function getBMI() {
        return $this->weight / ($this->height * $this->height);
    }
}
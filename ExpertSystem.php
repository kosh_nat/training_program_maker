<?php
/**
 * Created by PhpStorm.
 * User: knv
 * Date: 01.03.2018
 * Time: 14:17
 */

class ExpertSystem
{
    public $user = null;
    public $weight_norm = 100;

    // female - own weight;
    // male - more than own weight
    public $max_weight = null;

    // possible values:
    // 0 - underweight;
    // 1 - overweight;
    // 2 - normal weight;
    // 3 - obesity
    // 4 - morbid obesity
    public $weight_description = 0;

    // possible values:
    // 0 - weight loss;
    // 1 - weight gain;
    // 2 - health;
    public $training_purpose = 2;

    // possible values:
    // 0 - beginner;
    // 1 - medium level;
    // 2 - hight level;
    public $training_level = 0;

    public $strength_training = true;


    public function __construct($user) {
        $this->user = $user;
        $this->max_weight = $this->user->weight;
        $this->training_level = $this->user->training_level;
    }

    public function getWeightNorm() {
        if (($this->user->age >= 60) && ($this->user->age <= 16)) {
            $this->weight_norm = 70;
        }
    }

    public function getMaxWeight($method) {
        if($this->user->sex == 'male') {

        }
    }

    public function getWeightRecommendation() {
        $BMI = $this->user->BMI;

        if ($BMI > 25 && $BMI < 30 ) {
            $this->weight_description = 1;
        } elseif (($BMI >= 18.5) && ($BMI < 25)) {
            $this->weight_description = 2;
        } elseif($BMI >=30 && $BMI < 40) {
            $this->weight_description = 3;

            unsetStrengthTraining();
        } elseif ($BMI >= 40) {
            $this->weight_description = 4;

            unsetStrengthTraining();
        }
    }

    public function getTrainingPurpose($value) {
        $this->training_purpose = $value;

        return $this->training_purpose;
    }

    public function getRM($repetitions, $weight, $method) {
        $rm = null;
        switch ($method) {
            case 'lander':
                $rm = 100 * $weight / (101.3 - 2.67123 * $repetitions);
                break;
            case 'epley':
                $rm = $weight * (1 + $repetitions / 30);
                break;
            case 'brzycki':
                $rm = $weight / (1.0278 - (0.0278 * $repetitions));
                break;
            case 'lombardi':
                $rm = $weight * pow($repetitions, 0.1);
                break;
            case 'mayhew':
                $rm = 100 * $weight / (52.2 + 41.9 * exp(-0.055 * $repetitions));
                break;
            case 'oconner':
                $rm = $weight * (1 + 0.025 * $repetitions);
                break;
            case 'wathan':
                $rm = 100 * $weight / (48.8 + 53.8 * exp(-0.075 * $repetitions));
                break;
        }

        return $rm;
    }

    public function unsetStrengthTraining() {
       $this->strength_training = false;
    }

    public function isMaternity() {
        if ($this->user->maternity == true) {
            unsetStrengthTraining();
        }
    }

    public function getHRR($type) {
        $hrr = array();
        $pulse = 220 - $this->user->age;

        switch ($type) {
            case 'anaerobic':
                array_push($hrr, $pulse * 0.85);
                array_push($hrr, $pulse * 1);
                break;
            case 'target_zone':
                array_push($hrr, $pulse * 0.65);
                array_push($hrr, $pulse * 0.85);
                break;
            case 'aerobic':
                array_push($hrr, $pulse * 0.50);
                array_push($hrr, $pulse * 0.65);
                break;
            case 'recovery':
                array_push($hrr, $pulse * 0.50);
                array_push($hrr, $pulse * 0.25);
                break;
        }

        return $hrr;
    }

    public function checkFemaleTestosterone($testosterone_value) {

    }

    

}